﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using Ontology_Module;
using OntologyClasses.BaseClasses;

namespace PortListenerForText_Module
{
    public class clsDataWork_PortListener
    {

        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_PortListener;
        private OntologyModDBConnector objDBLevel_PortConfiguration;
        private OntologyModDBConnector objDBLevel_Count;
        private OntologyModDBConnector objDBLevel_Seperator;


        public clsOntologyItem Port { get; private set; }
        public List<clsPortConfig> PortsOfResourceItems { get; private set; } 

        public clsOntologyItem GetData()
        {
            var searchPort = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Parent_Object = objLocalConfig.OItem_object_baseconfig.GUID_Parent,
                            ID_RelationType = objLocalConfig.OItem_relationtype_listens_on.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_port.GUID
                        }
                };

            var objOItem_Result = objDBLevel_PortListener.GetDataObjectRel(searchPort, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var baseConfigPorts =
                    objDBLevel_PortListener.ObjectRels.Where(
                        portItem => portItem.ID_Object == objLocalConfig.OItem_object_baseconfig.GUID).ToList();

                if (baseConfigPorts.Any())
                {
                    Port =
                        baseConfigPorts.OrderBy(port => port.OrderID)
                                               .Select(port => new clsOntologyItem
                                                   {
                                                       GUID = port.ID_Other,
                                                       Name = port.Name_Other,
                                                       GUID_Parent = port.ID_Parent_Other,
                                                       Type = objLocalConfig.Globals.Type_Object
                                                   }).ToList().First();

                    var lngPort = 0;

                    if (int.TryParse(Port.Name, out lngPort))
                    {
                        Port.Val_Long = lngPort;
                    }
                    else
                    {
                        Port = null;
                        objOItem_Result = objLocalConfig.Globals.LState_Error.Clone();
                    }
                }
                else
                {
                    objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone();
                }
            }

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = GetResourceItems();
                
            }

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = GetCount();
                
            }

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = GetSeperator();
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    PortsOfResourceItems = (from configItem in objDBLevel_PortListener.ObjectRels
                                            join resourceItem in objDBLevel_PortConfiguration.ObjectRels on configItem.ID_Object equals
                                                resourceItem.ID_Object into resourceItems
                                            from resourceItem in resourceItems.DefaultIfEmpty()
                                            join count in objDBLevel_Count.ObjAtts on configItem.ID_Object equals count.ID_Object into counts
                                            from count in counts.DefaultIfEmpty()
                                            join seperator in objDBLevel_Seperator.ObjectRels on configItem.ID_Object equals  seperator.ID_Object into seperators
                                            from seperator in seperators.DefaultIfEmpty()
                                            select new clsPortConfig
                                            {
                                                IdConfiguration = configItem.ID_Object,
                                                NameConfiguration = configItem.Name_Object,
                                                IdResourceItem = resourceItem != null ? resourceItem.ID_Other : null,
                                                NameResourceItem = resourceItem != null ?  resourceItem.Name_Other : null,
                                                IdParent_ResourceItem = resourceItem != null ? resourceItem.ID_Parent_Other : null,
                                                Type_ResourceItem = resourceItem != null ? resourceItem.Ontology : null,
                                                PortString = configItem.Name_Other,
                                                IdAttributeCount = count != null ? count.ID_Attribute : null,
                                                LineCountBeforeSaving = count != null ? count.Val_Lng.Value : 0,
                                                IdSeperator = seperator != null ? seperator.ID_Other : null,
                                                NameSeperator = seperator != null ? seperator.Name_Other : null
                                            }).ToList();
                }
            }

            return objOItem_Result;

        }

        private clsOntologyItem GetResourceItems()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchResources = objDBLevel_PortListener.ObjectRels.Where(
                configItem => configItem.ID_Object != objLocalConfig.OItem_object_baseconfig.GUID).Select(
                    configItem => new clsObjectRel
                    {
                        ID_Object = configItem.ID_Object,
                        ID_RelationType = objLocalConfig.OItem_relationtype_belonging_resource.GUID
                    }).ToList();


            objOItem_Result = objDBLevel_PortConfiguration.GetDataObjectRel(searchResources);

            return objOItem_Result;
        }

        private clsOntologyItem GetCount()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchCount = objDBLevel_PortListener.ObjectRels.Where(
                configItem => configItem.ID_Object != objLocalConfig.OItem_object_baseconfig.GUID).Select(
                    configItem => new clsObjectAtt
                    {
                        ID_Object = configItem.ID_Object,
                        ID_AttributeType =  objLocalConfig.OItem_attributetype_count.GUID
                    }).ToList();

            objOItem_Result = objDBLevel_Count.GetDataObjectAtt(searchCount);

            return objOItem_Result;
        }

        private clsOntologyItem GetSeperator()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchSeperator = objDBLevel_PortListener.ObjectRels.Where(
                configItem => configItem.ID_Object != objLocalConfig.OItem_object_baseconfig.GUID).Select(
                    configItem => new clsObjectRel
                    {
                        ID_Object = configItem.ID_Object,
                        ID_RelationType = objLocalConfig.OItem_relationtype_save_seperator.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_class_text_seperators.GUID
                    }).ToList();

            objOItem_Result = objDBLevel_Seperator.GetDataObjectRel(searchSeperator);

            return objOItem_Result;
        }

        public clsDataWork_PortListener(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_PortListener = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_PortConfiguration = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Seperator = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Count = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }

}
