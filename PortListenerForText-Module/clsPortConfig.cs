﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace PortListenerForText_Module
{
    [Flags]
    public enum SplitType
    {
        DisconnectOrSearchRange = 0,
        Count = 1,
        Seperator = 2
    }
    public class clsPortConfig
    {
        public long Port
        {
            get
            {
                long port;
                if (long.TryParse(PortString, out port))
                {
                    return port;
                }
                else
                {
                    return 0;

                }
            }
        }
        public string PortString { get; set; }
        public string IdResourceItem  { get; set; }
        public string NameResourceItem { get; set; }
        public string IdParent_ResourceItem { get; set; }
        public string Type_ResourceItem { get; set; }
        public string IdAttributeCount { get; set; }
        public long LineCountBeforeSaving { get; set; }
        public string IdSeperator { get; set; }
        public string NameSeperator { get; set; }
        public string IdConfiguration { get; set; }
        public string NameConfiguration { get; set; }

        public SplitType TypeOfSplit
        {
            get
            {
                if (!string.IsNullOrEmpty(NameSeperator))
                {
                    return SplitType.Seperator;
                }
                else if (LineCountBeforeSaving > 0)
                {
                    return SplitType.Count;
                }
                else
                {
                    return SplitType.DisconnectOrSearchRange;
                }
            }

        }

        

    }
}
