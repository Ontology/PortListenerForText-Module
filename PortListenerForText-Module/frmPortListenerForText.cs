﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace PortListenerForText_Module
{
    public partial class frmPortListenerForText : Form
    {
        private clsLocalConfig objLocalConfig;
        private clsDataWork_PortListener objDataWork_PortListener;

        private string log;
        private string output;
        private bool listen;
        private long port;

        private Thread threadServer;
        TcpListener server = null;

        private clsOntologyItem resourceItem = null;

        public delegate void TextFromStream(string line);

        public event TextFromStream textFromStream;

        public delegate void ClientClosed();

        public delegate void ListeningStarted();

        public delegate void ErrorListening();

        public delegate void DataIncoming(string endPort);

        public delegate void ErrorData();

        public event ClientClosed clientClosed;
        public event ListeningStarted listeningStarted;
        public event ErrorListening errorListening;
        public event DataIncoming dataIncoming;
        public event ErrorData dataError;

        private Boolean boolStreamed;
        private Boolean boolClose = false;
        public Boolean Connected { get; private set; }

        public clsPortConfig PortConfigurationBaseConfig
        {
            get { return objDataWork_PortListener.PortsOfResourceItems.FirstOrDefault(config => config.IdConfiguration == objLocalConfig.OItem_object_baseconfig.GUID); }
        }

        public clsPortConfig PortConfigurationResource
        {
            get
            {
                if (resourceItem != null)
                {
                    return
                        objDataWork_PortListener.PortsOfResourceItems.FirstOrDefault(
                            config => config.IdResourceItem == resourceItem.GUID);
                }
                else
                {
                    return null;
                }
                
            }
        }

        public frmPortListenerForText()
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            InitializeModule();
        }

        public frmPortListenerForText(clsLocalConfig localConfig)
        {
            InitializeComponent();

            objLocalConfig = localConfig;
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            InitializeModule();
        }

        public frmPortListenerForText(clsLocalConfig localConfig, clsOntologyItem resourceItem)
        {
            InitializeComponent();

            this.resourceItem = resourceItem;
            objLocalConfig = localConfig;
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            InitializeModule();
        }

        public frmPortListenerForText(Globals Globals)
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }
            
            boolStreamed = true;

            InitializeModule();
        }

        public frmPortListenerForText(Globals Globals, clsOntologyItem resourceItem)
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            boolStreamed = true;

            this.resourceItem = resourceItem;

        }

        private void InitializeModule()
        {
            objDataWork_PortListener = new clsDataWork_PortListener(objLocalConfig);

            var objOItem_Result = objDataWork_PortListener.GetData();

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                throw new Exception("Error while getting baseconfig!");

            }
            else if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
            {
                throw new Exception("Config-Error!");
            }
        }

        public void Initialize()
        {
           
            
            listen = false;
            log = "";
            output = "";
            port = 0;
            timer_Server.Stop();
            try
            {
                threadServer.Abort();
            }
            catch (Exception)
            {
                
            }

            if (objDataWork_PortListener.Port.Val_Long != null) port = (int)objDataWork_PortListener.Port.Val_Long;

            if (resourceItem != null)
            {
                var portItem =
                    objDataWork_PortListener.PortsOfResourceItems.FirstOrDefault(resourcePortItem => resourcePortItem.IdResourceItem == resourceItem.GUID);

                if (portItem != null)
                {
                    port = (int)portItem.Port;
                }
            }
            this.Text = "Port: " + port.ToString();
            threadServer = new Thread(RegisterTcpPort);
            threadServer.Start();
            timer_Server.Start();
        }

        private void RegisterTcpPort()
        {
            
            log = "";

            Connected = false;
            try
            {
                server.Server.Close();
            }
            catch (Exception)
            {
                
                
            }
            server = null;
            try
            {

                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localAddr, (int)port);
                server.Start();

                Connected = true;
                byte[] bytes = new byte[256];
                String data = null;             

                while (true)
                {

                    listen = true;
                    if (listeningStarted != null)
                    {
                        listeningStarted();    
                    }
                    
                    TcpClient client = server.AcceptTcpClient();
                    log = "Connected: " + client.Client.RemoteEndPoint.ToString();
                    if (dataIncoming != null)
                    {
                        dataIncoming(client.Client.RemoteEndPoint.ToString());
                    }
                    
                    data = null;


                    NetworkStream stream = client.GetStream();
                    int i;
                    StringBuilder sbData = new StringBuilder();

                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        sbData.Append(System.Text.Encoding.UTF8.GetString(bytes, 0, i));

                        
                        
                            
                            


                    }
                    if (textFromStream != null)
                    {
                        textFromStream(sbData.ToString());
                    }
                    client.Close();
                    if (clientClosed != null)
                    {
                        clientClosed();    
                    }
                    
                    
                }

               
            }
            catch (Exception ex)
            {
                if (clientClosed != null)
                {
                    clientClosed();    
                }
                
                Connected = false;
                log = "Registering-Error: " + ex.Message;
                if (dataError != null)
                {
                    dataError();
                }
            }

            if (boolStreamed)
            {
                boolClose = true;
            }


        }


        private void notifyIcon_Main_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPortListenerForText_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                server.Server.Close();
            }
            catch (Exception)
            {
                
                
            }
            if (clientClosed != null)
            {
                clientClosed();
            }
            
            notifyIcon_Main.Icon = null;
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_Server_Tick(object sender, EventArgs e)
        {
            if (listen)
            {
                toolStripLabel_ListenLog.Text = "L";
                toolStripLabel_ListenLog.BackColor = Color.Green;
            }
            else
            {
                toolStripLabel_ListenLog.Text = "-";
                toolStripLabel_ListenLog.BackColor = Color.Yellow;
            }

            if (!string.IsNullOrEmpty(output))
            {
                textBox_Output.Text = output;
            }

            if (!string.IsNullOrEmpty(log))
            {
                textBox_Log.Text = "Listen on Port " + objDataWork_PortListener.Port.Name;
                textBox_Log.Text += "\r\n" + log;
                log = "";
            }

            if (boolClose)
            {
                this.Close();
            }
        }
    }
}
